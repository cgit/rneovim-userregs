-- Implementation for the "fileregister" ("&). The fileregister is a user
-- register that reads the register data from a specific file
-- (g:fileregister_filename).
--
-- If the data is encoded in JSON, it is read as 'content' (see :help urf),
-- otherwise the file is interpreted as lines (useful when writing to the file
-- from an external program).
--
-- requires 'json-lua'

local userreg = require("vim.userreg")
local api = vim.api

local handler = {}

-- Retruns the filename to read/write to. Defaults to
-- /tmp/.vim.$USER.fileregister, but uses g:fileregister_filename if set.
function handler.get_file_name()
  return vim.g.fileregister_filename or
      '/tmp/.vim.' .. os.getenv("USER") .. ".fileregister"
end

-- Stores the yanked text into the fileregister as JSON.
function handler.do_yank(self, content)
  local json = require('JSON')
  local file = self.get_file_name();
  string = json:encode(content)

  file = io.open(file, "w")
  file:write(string)
  file:close()
end

-- Reads the text from the file register and returns it.
function handler.do_put(self)
  local file = self.get_file_name();
  local json = require('JSON')

  file = io.open(file, "r")

  if not file then
    return {}
  end

  content = file:read "*a"
  file:close();

  if content:sub(1, 1) == '{' then
    return json:decode(content)
  else
    ret = {}
    for s in content:gmatch("[^\r\n]+") do
        table.insert(ret, s)
    end
    return ret
  end
end

userreg.register_handler('&', handler)
