-- Implementation of the basename register.
--
-- The basename register is like the filename register ("%), but instead of
-- returning the whole filename, it will return just the basename with one
-- extension removed.
--
-- Useful for Java when needing to reference the class.
--
-- This register is assigned to the character '$'

local userreg = require("vim.userreg")
local api = vim.api

local handler = {}

function handler.do_yank(self, content)
  return -1 -- Read-only register.
end

function handler.do_put()
  return vim.fn.expand('%:t:r')
end

userreg.register_handler('$', handler)
