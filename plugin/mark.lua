-- Implementation of middle mark. This mark is always in the middle of the
-- viewport.

local api = vim.api

-- Compatibility check.
if not api.nvim_call_function('has', {'usermarks'}) then
  return nil
end

local usermark = require("vim.usermark")

-- The middle mark is on the middle of the screen.
local middle_mark = {}
function middle_mark.get(self)
  local topline = vim.api.nvim_call_function('line', {'w0'})
  local bottomline = vim.api.nvim_call_function('line', {'w$'})

  return math.floor((topline + bottomline) / 2)
end

-- Setting the middle mark sets the current cusor line to the middle of the
-- window. Principally this is the same as z.
function middle_mark.set(self)
  api.nvim_command('norm z.')
end

-- The top mark is the first line on the window. This does respect scrolloff.
local top_mark = {}
function top_mark.get(self)
  return vim.api.nvim_call_function('line', {'w0'}) + vim.o.scrolloff
end
-- Setting the top mark move the current line to the top. Principally this is
-- the same mas z<cr>
function top_mark.set(self)
  api.nvim_command('norm z<cr>')
end

-- Setting the bottom mark moves the current line to the bottom. Principally
-- this is the same as z-
local bottom_mark = {}
function bottom_mark.get(self)
  return vim.api.nvim_call_function('line', {'w$'}) - vim.o.scrolloff
end
-- Setting the bottom mark doesn't do anything.
function bottom_mark.set(self)
  api.nvim_command('norm z-')
end

-- Mark that's like the expression register, but for a mark.
local expr_mark = {}
function expr_mark.get(self)
  local mode = vim.api.nvim_call_function('mode', {})

  if mode == "c" then
    -- Avoid recurring asking for a value while in command mode.
    return nil
  end

  local expr = vim.api.nvim_call_function('input', {'='})
  if expr ~= "" then
    return vim.api.nvim_eval(expr)
  end

  return nil
end

-- Setting the expr_mark does'nt do anything
function expr_mark.set(self) end

-- Register all the marks with their respective characters.
usermark.register_handler('-', middle_mark)
usermark.register_handler('+', top_mark)
usermark.register_handler('_', bottom_mark)
usermark.register_handler('=', expr_mark)
